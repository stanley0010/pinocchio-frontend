import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  newsLetterBody: {
		width: "100%",
		height: "100%",
	},
	container: {
		height: "100%",
		width: "80%",
		margin: "auto",
		// backgroundColor: "red",
	},
	letterPicContainer: {
		width: "300px",
		margin: "0"
	},
	letterContainer: {
		// width: "40%",
		// border: "2px solid red",
		display: "flex",
		margin: "5px",
		// backgroundColor: "#F6F6F6",
	},
	letterContentContainer: {
		margin: "0 10px",
		// border: "2px solid red",
		paddingTop: "0px",
	},
	letterTitle: {

		fontWeight: "bold",
		fontSize: "25px",
		verticalAlign: "top",
		margin: "0"
		// backgroundColor: "green"
	},
	letterContent: {
		margin: "0",
	},
	letterInfo: {
		display: "flex",
		margin: "0",
		color: "#3B79B1",
	},
	title: {
		color: "red",
	}
}));  

export default useStyles;