import React from 'react';
import firebase from "firebase/compat/app";
import {
  Button,
  Grid,
  useMediaQuery,
  SwipeableDrawer,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  IconButton,
  Typography,
  Box,
  InputBase,
  Container,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
// import { Button } from "@mui/material";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Widgets } from "@material-ui/icons";

import { FetchNewsletter, FetchRecentNewsletter } from '../../utils/fetchNewsletter';

const Testing = () => {

  const HandleCreateNewsletter = () => {
    console.log("Create newsletter");

    var db = firebase.firestore();

    db.collection("newsletter").add({
      title: "Tips on how to change your mindset and boost self-esteem",
      content: "Nathaniel Branden, author of ‘The Six Pillars of Self-esteem’, says that “If self esteem can be a scale to measure one’s mental health, there is no other urgent topic to discuss”. As such, self-esteem is not just crucial for you to represent yourself towards others, but it also holds an important role in the general well being of your life.",
      fullHTML: `<h1><span style="background-color: transparent;">Tips on How to Change Your Mindset and Boost Self-Esteem</span></h1><p><br></p><p><span style="background-color: transparent;">Nathaniel Branden, author of ‘The Six Pillars of Self-esteem’, says that “If self esteem can be a scale to measure one’s mental health, there is no other urgent topic to discuss”. As such, self-esteem is not just crucial for you to represent yourself towards others, but it also holds an important role in the general well being of your life.</span></p><p><span style="background-color: transparent;">The author advises that resorting to the opinion of others as a main source of self-evaluation will be very dangerous. The reason for this is that first, the evaluation of others does not help with your self-esteem, and second, if you continue to rely on the evaluation of others, there is a risk of being addicted to being recognized by others. </span></p><p><span style="background-color: transparent;">I deeply sympathise with what he says; The more you rely on the evaluation of others, the higher the possibility that you would be living a life that can be easily dragged by others. When your own self-esteem becomes healthy, you will know that what matters more is the opinion of your own self, rather than that of others.</span></p><p><span style="background-color: transparent;">Now, what would be some methods to transition our mindset to that will help us to improve our self-esteem?</span></p><p><br></p><h2><strong style="background-color: transparent;">1. Find out what you like and what you are good at</strong></h2><p><span style="background-color: transparent;">One common aspect of people with low self-esteem is that they </span><strong style="background-color: transparent;">don't know much about themselves. </strong><span style="background-color: transparent;">To put it more accurately, they have rarely thought deeply about themselves; of their own interest, own hobby, and so much more. If you start to “</span><strong style="background-color: transparent;">venture”</strong><span style="background-color: transparent;"> upon what you truly have a </span><strong style="background-color: transparent;">passion</strong><span style="background-color: transparent;"> for, you will start to care so much more about your own happiness. So, give yourself a little assignment; check </span><strong style="background-color: transparent;">what you like </strong><span style="background-color: transparent;">and </span><strong style="background-color: transparent;">what you don't like</strong><span style="background-color: transparent;">, find out what you are </span><strong style="background-color: transparent;">good</strong><span style="background-color: transparent;"> at, and when you are </span><strong style="background-color: transparent;">happy</strong><span style="background-color: transparent;"> and </span><strong style="background-color: transparent;">content.</strong></p><p><br></p><h2><strong style="background-color: transparent;">2. Set small goals rather than a big one and feel the sense of accomplishment often</strong></h2><p><span style="background-color: transparent;">Achieving small things first in your daily life and feeling successful is very </span><strong style="background-color: transparent;">effective</strong><span style="background-color: transparent;"> in improving your self-esteem. Nathaniel Brandon says that self-esteem has two closely related elements: </span><strong style="background-color: transparent;">self-efficacy</strong><span style="background-color: transparent;"> and</span><strong style="background-color: transparent;"> self-respect.</strong><span style="background-color: transparent;"> Self-efficacy is the confidence and belief that is necessary when you are faced with a challenge. To develop faith in yourself, you need the experience of achieving your own goals from the very small ones. Small sense of accomplishment builds up to further strengthen trust in yourself. </span></p><p> </p><p><span style="background-color: transparent;">I hope that these two pieces of advice can help you to better develop your own self-esteem. Remember, at the end of the day, nothing matters more than your own </span><strong style="background-color: transparent;">happiness</strong><span style="background-color: transparent;"> of your own decisions.</span></p>`,
      date: "30 April 2022",
      tags: ["self-esteem", "motivation"],
    })
      .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        console.log("Document written with ID: ", docRef);
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      }
      );
  }

  // const HandleFetchNewsletter = async () => {
  //   console.log("Fetch newsletter");

  //   var db = firebase.firestore();

  //   const result = await db.collection("newsletter").get().then((querySnapshot) => {
  //     querySnapshot.forEach((doc) => {
  //       // doc.data() is never undefined for query doc snapshots
  //       console.log(doc.id, " => ", doc.data());
  //     });
  //   });

  //   // console.log('result', result); // undefined
  // }

  // const HandleFetchRecentNewsletter = async () => {
  //   console.log("Fetch 3 most recent newsletter");

  //   var db = firebase.firestore();

  //   const result = await db
  //     .collection("newsletter")
  //     .orderBy("date", 'desc')
  //     .limit(3)
  //     .get()
  //     .then((querySnapshot) => {
  //       querySnapshot.forEach((doc) => {
  //         // doc.data() is never undefined for query doc snapshots
  //         console.log(doc.id, " => ", doc.data());
  //       });
  //     });
  // }

  const HandleFetchNewsletter = async () => {
    console.log("Fetch newsletter");
    const result = await FetchNewsletter();

    // FetchNewsletter().then((result) => {
    //   console.log('result', result);
    // });

    console.log('result', result);
  }

  const HandleFetchRecentNewsletter = async () => {
    console.log("Fetch 3 most recent newsletter");
    const result = await FetchRecentNewsletter();

    console.log('result', result);

  }

  return (
    <Box
      style={{
        width: "100%",
        height: '100vh',
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Typography>Testing</Typography>

      <Box
        style={{
          display: "flex",
          gap: '20px',
        }}
      >
        <Button
          style={{
            border: '1px solid black',
          }}
          onClick={HandleCreateNewsletter}
        >
          <Typography>Create newsletter</Typography>
        </Button>

        <Button
          style={{
            border: '1px solid black',
          }}
          onClick={HandleFetchNewsletter}
        >
          <Typography>Fetch newsletter</Typography>
        </Button>

        <Button
          style={{
            border: '1px solid black',
          }}
          onClick={HandleFetchRecentNewsletter}
        >
          <Typography>Fetch 3 most recent newsletter</Typography>
        </Button>
      </Box>


    </Box>
  )
}

export default Testing;