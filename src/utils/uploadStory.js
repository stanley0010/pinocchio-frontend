import firebase from "firebase/compat/app";
import { encodeUserUID } from "../utils/auth";

// import config from "../notification/ElasticEmail";

// Notification setup
const handleNotification = (storyTitle, creatorEmail) => {
  console.log("handleNotification");
  window.Email.send({
    Host: 'smtp.elasticemail.com',
    Port: '2525',
    Username: 'tommywong.ust@gmail.com',
    Password: 'D5B90F6BA7E61931BD028B9147965B6C9244',
    To : 'tommywong.ust@gmail.com',
    From : "tommywong.ust@gmail.com",
    Subject : "Notification Test from Pinocchio",
    Body : 
    `
      <h1>There is a new story</h1>
      <p>Story title: </p>
      <i>${storyTitle}</i>
      <p>Created from: </p>
      <i>${creatorEmail}</i>
    `,
  });
}

export function uploadStory(userUID, storyText, title, category, isPublic) {
  var db = firebase.firestore();
  db.collection("stories")
    .add({
      bookmarkUsersID: [],
      category: category,
      comments: [],
      isPublic: isPublic,
      text: storyText,
      createdTime: firebase.firestore.Timestamp.now(),
      title: title,
      userIDEncoded: encodeUserUID(userUID),
      userID: userUID,
      creatorEmail: firebase.auth().currentUser.email,
      weighting:
        category === "Motivation"
          ? Math.floor(Math.random() * 50) + 1
          : Math.floor(Math.random() * 40) + 20,
    })
    .then((documentRef) =>
      db
        .collection("users")
        .doc(encodeUserUID(userUID))
        .update({
          storiesID: firebase.firestore.FieldValue.arrayUnion(
            documentRef.id + (isPublic ? "PUBLIC" : "PRIVATE")
          ),
        })
    )
    .catch((error) => console.log(error));

    // Notification for each new public story
    var creatorEmail = firebase.auth().currentUser.email;
    handleNotification(title, creatorEmail);
}

export function uploadToxicStory(
  userUID,
  storyText,
  title,
  category,
  titleToxicity,
  textToxicity
) {
  var db = firebase.firestore();
  db.collection("toxicStories")
    .add({
      bookmarkUsersID: [],
      category: category,
      comments: [],
      text: storyText,
      createdTime: firebase.firestore.Timestamp.now(),
      title: title,
      userIDEncoded: encodeUserUID(userUID),
      userID: userUID,
      textToxicity: textToxicity,
      titleToxicity: titleToxicity,
    })
    .catch((error) => console.log(error));
}
