import React, { useState } from 'react';
import { Fab } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useStyles = makeStyles((theme) => ({
  scrollToTop: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: '#000000',
    '&:hover': {
        backgroundColor: '#FFF0DB', // replace 'hoverColorYouWant' with the color you want on hover
      },

  },
}));

const ScrollTop = () => {
  const classes = useStyles();
  const [visible, setVisible] = useState(false);

  const checkScrollTop = () => {
    if (!visible && window.pageYOffset > 400){
      setVisible(true);
    } else if (visible && window.pageYOffset <= 400){
      setVisible(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({top: 0, behavior: 'smooth'});
  };

  window.addEventListener('scroll', checkScrollTop);

  return (
    <Fab color="secondary" size="small" aria-label="scroll back to top" className={classes.scrollToTop} onClick={scrollTop} style={{display: visible ? 'flex' : 'none'}}>
      <KeyboardArrowUpIcon color = "#FFFFFF"/>
    </Fab>
  );
}

export default ScrollTop;