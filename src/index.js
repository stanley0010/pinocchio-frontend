import React from "react";
import ReactDOM from "react-dom";
import firebase from "firebase/compat/app";
import 'firebase/compat/storage';
import { Suspense } from "react";
import "firebase/compat/analytics";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import "./index.css";
// import { createMuiTheme } from "@material-ui/core/styles";
import { createTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
// import { ThemeProvider } from '@mui/material';
// import CssBaseline from "@mui/material/CssBaseline";

import * as serviceWorkerRegistration from "./serviceWorkerRegistration";
import reportWebVitals from "./reportWebVitals";
import Router from "./containers/RouteCollection";
import store from "./store";
import { Provider } from "react-redux";
import "./i18n";

// import Swiper from 'swiper';
// import { Navigation, Pagination } from 'swiper/modules';

// // // import styles bundle
// // import 'swiper/css/bundle';

// // init Swiper:
// const swiper = new Swiper('.swiper', {
//   // configure Swiper to use modules
//   modules: [Navigation, Pagination],
// });


const theme = createTheme({
  typography: {
    fontFamily: ["Open Sans", "sans-serif"].join(","),
  },
});

const firebaseConfig = {
  apiKey: "AIzaSyDiNVv8_UBM0Egcbxqjaf0K51x-9J_sX0A",
  authDomain: "ust-sight-pinocchio.firebaseapp.com",
  projectId: "ust-sight-pinocchio",
  storageBucket: "ust-sight-pinocchio.appspot.com",
  messagingSenderId: "449896953766",
  appId: "1:449896953766:web:1f5374d846ffb070320167",
  measurementId: "G-NPZZZP04E6",
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Suspense fallback='...is loading'>
          <Router />
        </Suspense>
      </Provider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
